import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>
          Group 11 - Devops (NEW CHANGE)
        </h1>
        <pre>
          Vineeta (1814066)
          Akshit (1814067)
          Rajat (1814068)
          Sounak (1814070)
          Dharmil (1814076)
          Neha (1814098)
        </pre>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
